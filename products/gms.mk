#
# Copyright (C) 2020 The LineageOS Project
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.
#

# Fonts
PRODUCT_COPY_FILES += \
    vendor/gms/products/fonts_customization.xml:$(TARGET_COPY_OUT_PRODUCT)/etc/fonts_customization.xml

# Overlay
PRODUCT_PACKAGES += \
    ContactsProviderOverlay \
    FontGoogleSansOverlay \
    GmsConfigOverlay \
    LatinIMEGoogleOverlay \
    TelecommOverlay

# Overlay for Turbo
PRODUCT_PACKAGE_OVERLAYS += vendor/gms/overlay/turbo_overlay

# SetupWizard
PRODUCT_PRODUCT_PROPERTIES += \
    ro.setupwizard.rotation_locked=true \
    setupwizard.theme=glif_v3_light

$(call inherit-product, vendor/gms/common/common-vendor.mk)
